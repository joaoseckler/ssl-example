#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <pthread.h>
#include "openssl/ssl.h"
#include "openssl/err.h"


int OpenListener(int port) {
  int sd;
  struct sockaddr_in addr;

  sd = socket(AF_INET, SOCK_STREAM, 0);
  memset(&addr, 0, sizeof(addr));

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = INADDR_ANY;

  if (bind(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
    perror("can't bind port");
    abort();
  }

  if (listen(sd, 10) != 0) {
    perror("I'm deaf! Can't listen!");
    abort();
  }
  return sd;
}

SSL_CTX* InitServerCTX() {
  const SSL_METHOD *method;
  SSL_CTX *ctx;

  OpenSSL_add_all_algorithms();
  SSL_load_error_strings();
  method = TLS_server_method();
  ctx = SSL_CTX_new(method);

  if (!ctx) {
    ERR_print_errors_fp(stderr);
    abort();
  }

  return ctx;
}

void LoadCertificates(SSL_CTX *ctx, char* certfilename, char* keyfilename) {
  if (SSL_CTX_use_certificate_file(ctx, certfilename, SSL_FILETYPE_PEM) <= 0) {
    ERR_print_errors_fp(stderr);
    abort();
  }
  if (!SSL_CTX_use_PrivateKey_file(ctx, keyfilename, SSL_FILETYPE_PEM)) {
    ERR_print_errors_fp(stderr);
    abort();
  }
  if (!SSL_CTX_check_private_key(ctx)) {
    fprintf(stderr, "Private key does not match the public certificate\n");
    abort();
  }
}

void ShowCerts(SSL* ssl) {
  X509 *cert;
  char *line;
  cert = SSL_get_peer_certificate(ssl); /* Get certificates (if available) */
  if ( cert != NULL )
  {
    printf("Server certificates:\n");
    line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
    printf("Subject: %s\n", line);
    free(line);
    line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
    printf("Issuer: %s\n", line);
    free(line);
    X509_free(cert);
  }
  else
    printf("No certificates.\n");
}

void *Servlet(void *arg) {
  SSL *ssl;
  int sd, bytes;
  char buf[512] = {0}, buf2[1024] = {0};

  ssl = (SSL*)arg;

  if (SSL_accept(ssl) <= 0) {
    ERR_print_errors_fp(stderr);
  }
  else {
    ShowCerts(ssl);
    /* do something here. Use SSL_read and SSL_write */
    while (1) {
      bytes = SSL_read(ssl, buf, 1024);
      buf[bytes - 1] = 0;
      printf("Received: %s\n", buf);
      sprintf(buf2, "You sent me this: %s. Thank you very much!\n", buf);
      SSL_write(ssl, buf2, strlen(buf2) + 1);
    }
  }

  sd = SSL_get_fd(ssl);
  SSL_free(ssl);
  close(sd);
  return NULL;
}

int main(int argc, char **argv) {
  SSL_CTX *ctx;
  int server, client;
  char *portnum;
  struct sockaddr_in addr;
  socklen_t len = sizeof(addr);
  SSL *ssl;
  pthread_t pt;
  pthread_attr_t pattr;

  pthread_attr_init(&pattr);
  pthread_attr_setdetachstate(&pattr, PTHREAD_CREATE_DETACHED);

  if (argc != 2) {
    printf("Usage: %s <portnum>\n", argv[0]);
    exit(0);
  }

  SSL_library_init();
  portnum = argv[1];
  ctx = InitServerCTX();
  LoadCertificates(ctx, "mycert.pem", "mycert.pem");
  server = OpenListener(atoi(portnum));

  while(1) {
    client = accept(server, (struct sockaddr *)&addr, &len);
    printf("Connection: %s:%d\n",inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
    ssl = SSL_new(ctx);
    SSL_set_fd(ssl, client);
    pthread_create(&pt, &pattr, Servlet, (void*)ssl);
  }
}

