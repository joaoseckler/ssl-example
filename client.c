#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <sys/socket.h>
#include <resolv.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

int OpenConnection(const char *hostname, int port) {
  int sd;
  struct hostent *host;
  struct sockaddr_in addr;


  if ((host = gethostbyname(hostname)) == NULL) {
    perror(hostname);
    abort();
  }

  sd = socket(AF_INET, SOCK_STREAM, 0);
  memset(&addr, 0, sizeof(addr));

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = *(long*)(host->h_addr);

  if (connect(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
    close(sd);
    perror(hostname);
    abort();
  }
  return sd;
}

SSL_CTX* InitCTX(void) {
  const SSL_METHOD *method;
  SSL_CTX *ctx;

  OpenSSL_add_all_algorithms();
  SSL_load_error_strings();
  method = TLS_client_method();
  ctx = SSL_CTX_new(method);

  if (ctx == NULL) {
    ERR_print_errors_fp(stderr);
    abort();
  }

  return ctx;
}

void ShowCerts(SSL *ssl) {
  X509 *cert;
  char *line;

  cert = SSL_get_peer_certificate(ssl);
  if (cert != NULL) {
    printf("Server certificates:\n");
    line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
    printf("Subject: %s\n", line);
    free(line);
    line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
    printf("Issuer: %s\n", line);
    free(line);
    X509_free(cert);
  }
  else
    printf("Info: No client certificates configured.\n");
}

int main(int argc, char **argv) {
  SSL_CTX *ctx;
  int server, ret;
  SSL *ssl;
  char *line = NULL, *hostname, *portnum;
  ssize_t line_n = 0;

  if (argc != 3) {
    printf("Check your argc!\n");
    exit(0);
  }

  SSL_library_init();
  hostname = argv[1];
  portnum = argv[2];
  ctx = InitCTX();
  server = OpenConnection(hostname, atoi(portnum));
  ssl = SSL_new(ctx);
  SSL_set_fd(ssl, server);
  if ((ret = SSL_connect(ssl)) < 0)
    SSL_get_error(ssl, ret);
  else
  {
    while (1) {
      getline(&line, (size_t*)&line_n, stdin);
      SSL_write(ssl, line, (int)line_n);
      line_n += 100;
      line = realloc(line, (size_t)line_n);
      SSL_read(ssl, line, line_n);
      printf("%s\n", line);
      free(line);
    }
  }
  close(server);
  SSL_CTX_free(ctx);
  return 0;
}
