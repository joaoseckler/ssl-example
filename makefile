CFLAGS=-Wall -pedantic -Wextra -g
CXXFLAGS=-std=c++17 -Wall -pedantic -Wextra -g
LDLIBS=-lpthread -lssl -lcrypto

all: client server mycert.pem

client:

server:

mycert.pem:
	openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout mycert.pem -out mycert.pem

clean:
	rm -rf *.o client server
